from flask import Blueprint, jsonify, request
from .. import persistence

program_blueprint = Blueprint(name="program_blueprint", import_name=__name__)


@program_blueprint.route('/<int:id>', methods=['GET'])
def get_program(id):
    """
    ---
    get:
      description: get endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: ProgramOutputSchema
      tags:
          - Program Endpoints
      parameters:
      - in: path
        schema:
          type: integer
        name: id
    """
    db = persistence.get_db()
    programs = db.execute(
        'SELECT p.id, p.name, p.description FROM program p WHERE p.id = ?', (int(id),)
    ).fetchall()
    payload = []
    for program in programs:
        payload.append({'id': program[0], 'program': program[1], 'description': program[2]})
    return jsonify(payload)


@program_blueprint.route('/', methods=['POST'])
def set_program():
    """
    ---
    post:
      description: set endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: ProgramOutputSchema
      tags:
          - Program Endpoints
    """
    db = persistence.get_db()
    request_json = request.json
    db.execute(
        'INSERT INTO program (name, description) VALUES (?, ?)',
        (request_json["program"], request_json["description"])
    )
    db.commit()
    results = db.execute(
        'SELECT last_insert_rowid()'
    ).fetchone()
    last_inserted_rowid = 0
    for result in results:
        last_inserted_rowid = result
    programs = db.execute(
        'SELECT p.id, p.name, p.description FROM program p WHERE p.id = ?', (int(last_inserted_rowid),)
    ).fetchall()
    payload = []
    for program in programs:
        payload.append({'id': program[0], 'program': program[1], 'description': program[2]})
    return jsonify(payload)


@program_blueprint.route('/<int:id>', methods=['DELETE'])
def delete_program(id):
    """
    ---
    delete:
      description: delete endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: DeleteOutputSchema
      tags:
          - Program Endpoints
      parameters:
      - in: path
        schema:
          type: integer
        name: id
    """
    db = persistence.get_db()
    db.execute('DELETE FROM program WHERE id = ?', (id,))
    db.commit()
    payload = {'status': 'deleted'}
    return jsonify(payload)
