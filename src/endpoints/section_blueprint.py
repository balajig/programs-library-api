from flask import Blueprint, jsonify, request
from .. import persistence

section_blueprint = Blueprint(name="section_blueprint", import_name=__name__)


@section_blueprint.route('/<int:id>', methods=['GET'])
def get_section(id):
    """
    ---
    get:
      description: get endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: SectionOutputSchema
      tags:
          - Section Endpoints
      parameters:
      - in: path
        schema:
          type: integer
        name: id
    """
    db = persistence.get_db()
    sections = db.execute(
        'SELECT s.id, s.name, s.description, s.overview_image, s.order_index, s.program_id'
        ' FROM section s WHERE s.id = ?', (int(id),)
    ).fetchall()
    payload = []
    for section in sections:
        payload.append({'id': section[0], 'section': section[1], 'description': section[2],
                        'overview_image': section[3], 'order_index': section[4], 'program_id': section[5]})
    return jsonify(payload)


@section_blueprint.route('/', methods=['POST'])
def set_section():
    """
    ---
    post:
      description: set endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: SectionOutputSchema
      tags:
          - Section Endpoints
    """
    db = persistence.get_db()
    request_json = request.json
    db.execute(
        'INSERT INTO section (name, description, overview_image, order_index, program_id)'
        ' VALUES (?, ?, ?, ?, ?)',
        (request_json["section"], request_json["description"], request_json["overview_image"],
         request_json["order_index"], request_json["program_id"])
    )
    db.commit()
    results = db.execute(
        'SELECT last_insert_rowid()'
    ).fetchone()
    last_inserted_rowid = 0
    for result in results:
        last_inserted_rowid = result
    sections = db.execute(
        'SELECT s.id, s.name, s.description, s.overview_image, s.order_index, s.program_id'
        ' FROM section s WHERE s.id = ?', (int(last_inserted_rowid),)
    ).fetchall()
    payload = []
    for section in sections:
        payload.append({'id': section[0], 'section': section[1], 'description': section[2],
                        'overview_image': section[3], 'order_index': section[4], 'program_id': section[5]})
    return jsonify(payload)


@section_blueprint.route('/<int:id>', methods=['DELETE'])
def delete_section(id):
    """
    ---
    delete:
      description: delete endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: DeleteOutputSchema
      tags:
          - Section Endpoints
      parameters:
      - in: path
        schema:
          type: integer
        name: id
    """
    db = persistence.get_db()
    db.execute('DELETE FROM section WHERE id = ?', (id,))
    db.commit()
    payload = {'status': 'deleted'}
    return jsonify(payload)
