from flask import Blueprint, jsonify, request
from .. import persistence

activity_blueprint = Blueprint(name="activity_blueprint", import_name=__name__)


@activity_blueprint.route('/<int:id>', methods=['GET'])
def get_activity(id):
    """
    ---
    get:
      description: get endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: ActivityOutputSchema
      tags:
          - Activity Endpoints
      parameters:
      - in: path
        schema:
          type: integer
        name: id
    """
    db = persistence.get_db()
    activities = db.execute(
        'SELECT a.id, a.content, a.content_type, a.section_id FROM activity a WHERE a.id = ?', (int(id),)
    ).fetchall()
    payload = []
    for activity in activities:
        payload.append({'id': activity[0], 'content': activity[1],
                        'content_type': activity[2], 'section_id': activity[3]})
    return jsonify(payload)


@activity_blueprint.route('/', methods=['POST'])
def set_activity():
    """
    ---
    post:
      description: set endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: ActivityOutputSchema
      tags:
          - Activity Endpoints
    """
    db = persistence.get_db()
    request_json = request.json
    db.execute(
        'INSERT INTO activity (content, content_type, section_id) VALUES (?, ?, ?)',
        (request_json["content"], request_json["content_type"], request_json["section_id"])
    )
    db.commit()
    results = db.execute(
        'SELECT last_insert_rowid()'
    ).fetchone()
    last_inserted_rowid = 0
    for result in results:
        last_inserted_rowid = result
    activities = db.execute(
        'SELECT a.id, a.content, a.content_type, a.section_id FROM activity a WHERE a.id = ?',
        (int(last_inserted_rowid),)
    ).fetchall()
    payload = []
    for activity in activities:
        payload.append({'id': activity[0], 'content': activity[1],
                        'content_type': activity[2], 'section_id': activity[3]})
    return jsonify(payload)


@activity_blueprint.route('/<int:id>', methods=['DELETE'])
def delete_activity(id):
    """
    ---
    delete:
      description: delete endpoint
      responses:
        '200':
          description: call successful
          content:
            application/json:
              schema: DeleteOutputSchema
      tags:
          - Activity Endpoints
      parameters:
      - in: path
        schema:
          type: integer
        name: id
    """
    db = persistence.get_db()
    db.execute('DELETE FROM activity WHERE id = ?', (id,))
    db.commit()
    payload = {'status': 'deleted'}
    return jsonify(payload)
