pragma foreign_keys = on;

drop table if exists activity;
drop table if exists section;
drop table if exists program;

create table program (
  id integer primary key autoincrement,
  name text not null,
  description text not null
);

create table section (
  id integer primary key autoincrement,
  name text not null,
  description text not null,
  overview_image text not null,
  order_index integer not null,
  program_id integer not null,
  foreign key(program_id) references program(id)
);

create table activity (
  id integer primary key autoincrement,
  content text not null,
  content_type text not null,
  section_id integer not null,
  foreign key(section_id) references section(id)
);