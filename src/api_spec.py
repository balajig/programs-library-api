from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from marshmallow import Schema, fields

spec = APISpec(
    title="Programs Library API",
    version="1.0.0",
    openapi_version="3.0.2",
    plugins=[FlaskPlugin(), MarshmallowPlugin()],
)

ProgramOutputSchema = Schema.from_dict(
    {
        "id": fields.Number(),
        "name": fields.Str(),
        "description": fields.Str()
    }
)

SectionOutputSchema = Schema.from_dict(
    {
        "id": fields.Number(),
        "name": fields.Str(),
        "description": fields.Str(),
        "overview_image": fields.Str(),
        "order_index": fields.Str(),
        "program_id": fields.Number(),
    }
)

ActivityOutputSchema = Schema.from_dict(
    {
        "id": fields.Number(),
        "content": fields.Str(),
        "content_type": fields.Str(),
        "section_id": fields.Number(),
    }
)

DeleteOutputSchema = Schema.from_dict(
    {
        "status": fields.Str()
    }
)

spec.components.schema("ProgramOutputSchema", schema=ProgramOutputSchema)
spec.components.schema("SectionOutputSchema", schema=SectionOutputSchema)
spec.components.schema("ActivityOutputSchema", schema=ActivityOutputSchema)
spec.components.schema("DeleteOutputSchema", schema=DeleteOutputSchema)

tags = [
    {'name': 'Program Endpoints',
     'description': 'For accessing Program data'
     },
    {'name': 'Section Endpoints',
     'description': 'For accessing Section data'
     },
    {'name': 'Activity Endpoints',
     'description': 'For accessing Activity data'
     },
]

for tag in tags:
    print(f"Adding tag: {tag['name']}")
    spec.tag(tag)
