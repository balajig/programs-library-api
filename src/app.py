from flask import Flask, jsonify

from . import persistence
import os

from src.endpoints.swagger import swagger_ui_blueprint, SWAGGER_URL
from src.endpoints.section_blueprint import section_blueprint
from src.endpoints.activity_blueprint import activity_blueprint
from src.endpoints.program_blueprint import program_blueprint

app = Flask(__name__)
app.config.from_mapping(
    DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
)

app.register_blueprint(program_blueprint, url_prefix="/api/program/v1/")
app.register_blueprint(section_blueprint, url_prefix="/api/section/v1/")
app.register_blueprint(activity_blueprint, url_prefix="/api/activity/v1/")

from src.api_spec import spec

with app.test_request_context():
    for fn_name in app.view_functions:
        if fn_name == 'static':
            continue
        print(f"Loading swagger docs for function: {fn_name}")
        view_fn = app.view_functions[fn_name]
        spec.path(view=view_fn)


@app.route("/api/swagger.json")
def create_swagger_spec():
    return jsonify(spec.to_dict())


app.register_blueprint(swagger_ui_blueprint, url_prefix=SWAGGER_URL)

persistence.init_app(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
