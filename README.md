## Setup

How to set up and run the dev server and prod server

1. Clone the repo - git clone https://bitbucket.org/balajig/programs-library-api.git
2. Goto {workspace}/programs-library-api
3. Run this to setup SQLite DB with programs-library schema - flask init-db
4. Run this start the dev server - ./run_flask_dev.sh
5. Open another console window and run this to test the API - ./test_flask_dev.sh
6. For prod server run these - ./run_flask_prod.sh and ./test_flask_prod.sh
