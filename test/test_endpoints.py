import os

import pytest
import requests
from openapi_spec_validator import validate_spec_url

program_id = 0
section_id = 0
activity_id = 0


@pytest.mark.dependency()
def test_post_program_v1(api_v1_host):
    endpoint = os.path.join(api_v1_host, 'program/v1')
    payload = {'program': 'some_program', 'description': 'some_description'}
    response = requests.post(endpoint, json=payload)
    assert response.status_code == 200
    json = response.json()
    assert json[0]['id']
    global program_id
    program_id = json[0]['id']


@pytest.mark.dependency(depends=['test_post_program_v1'])
def test_get_program_v1(api_v1_host):
    global program_id
    endpoint = os.path.join(api_v1_host, 'program/v1/{0}'.format(program_id))
    response = requests.get(endpoint)
    assert response.status_code == 200
    json = response.json()
    assert json[0]['id'] == program_id
    assert json[0]['program'] == 'some_program'
    assert json[0]['description'] == 'some_description'


@pytest.mark.dependency(depends=['test_get_program_v1'])
def test_post_section_v1(api_v1_host):
    endpoint = os.path.join(api_v1_host, 'section/v1')
    global program_id
    payload = {'section': 'some_section', 'description': 'some_description',
               'overview_image': 'some_overview_image', 'order_index': 'some_order_index',
               'program_id': program_id}
    response = requests.post(endpoint, json=payload)
    assert response.status_code == 200
    json = response.json()
    assert json[0]['id']
    global section_id
    section_id = json[0]['id']


@pytest.mark.dependency(depends=['test_post_section_v1'])
def test_get_section_v1(api_v1_host):
    global section_id
    endpoint = os.path.join(api_v1_host, 'section/v1/{0}'.format(section_id))
    response = requests.get(endpoint)
    assert response.status_code == 200
    json = response.json()
    assert json[0]['id'] == section_id
    assert json[0]['section'] == 'some_section'
    assert json[0]['description'] == 'some_description'


@pytest.mark.dependency(depends=['test_get_section_v1'])
def test_post_activity_v1(api_v1_host):
    global section_id
    endpoint = os.path.join(api_v1_host, 'activity/v1')
    payload = {'content': 'some_content', 'content_type': 'some_content_type',
               'section_id': section_id}
    response = requests.post(endpoint, json=payload)
    assert response.status_code == 200
    json = response.json()
    assert json[0]['id']
    global activity_id
    activity_id = json[0]['id']


@pytest.mark.dependency(depends=['test_post_activity_v1'])
def test_get_activity_v1(api_v1_host):
    global activity_id
    endpoint = os.path.join(api_v1_host, 'activity/v1/{0}'.format(activity_id))
    response = requests.get(endpoint)
    assert response.status_code == 200
    json = response.json()
    assert json[0]['id'] == activity_id
    assert json[0]['content'] == 'some_content'
    assert json[0]['content_type'] == 'some_content_type'


@pytest.mark.dependency(depends=['test_get_activity_v1'])
def test_delete_activity_v1(api_v1_host):
    global section_id
    endpoint = os.path.join(api_v1_host, 'activity/v1/{0}'.format(section_id))
    response = requests.delete(endpoint)
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'deleted'


@pytest.mark.dependency(depends=['test_delete_activity_v1'])
def test_delete_section_v1(api_v1_host):
    global section_id
    endpoint = os.path.join(api_v1_host, 'section/v1/{0}'.format(section_id))
    response = requests.delete(endpoint)
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'deleted'


@pytest.mark.dependency(depends=['test_delete_section_v1'])
def test_delete_program_v1(api_v1_host):
    global program_id
    endpoint = os.path.join(api_v1_host, 'program/v1/{0}'.format(program_id))
    response = requests.delete(endpoint)
    assert response.status_code == 200
    json = response.json()
    assert json['status'] == 'deleted'


def test_swagger_specification(host):
    endpoint = os.path.join(host, 'api', 'swagger.json')
    validate_spec_url(endpoint)
